#include "pratique.c"
#include <stdio.h>
#include <stdlib.h>

int maximum(int *tab, int taille)
{
    int max = *tab;
    for (int i = 1; i < taille; i++)
    {
        max = max > tab[i] ? max : tab[i];
    }

    return max;
}

int question1()
{
    int tab[3];
    tab[0] = -5;
    tab[1] = 4;
    tab[2] = 3;

    printint(maximum(tab, 3));
}

void question2()
{
    // Il est impossible de connaitre la taille des tableau à l'avance,
    // c'est pour cela que l'on donne sa taille en paramètre
}

void question3()
{
    // Ici on indique en paramètre de f les adresses auxquels doivent
    // etre mis les resultat
}

void minmax(int *tab, int taille, int *pmin, int *pmax)
{
    int min = *tab;
    int max = *tab;

    for (int i = 1; i < taille; i++)
    {
        min = min < tab[i] ? min : tab[i];
        max = min > tab[i] ? max : tab[i];
    }

    *pmin = min;
    *pmax = max;
}

void question5()
{
    int tab[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    int minimum, maximum;
    minmax(tab, 10, &minimum, &maximum);

    printf("Min = %i, max = %i", minimum, maximum);
}

/* int *copie(int *tab, int n)
{
    int tab2[n];
    for (int i = 0; i < n; i++)
    {
        tab2[i] = tab[i];
    }
    return tab2;
} */

int *copie2(int *tab, int n)
{
    int *res = malloc(sizeof(int) * n);
    for (int i = 0; i < n; i++)
    {
        res[i] = tab[i];
    }

    return res;
}

void question7()
{
    int tab[5] = {1, 2, 3, 4, 5};
    int *tab2 = copie2(tab, 5);
    printint(tab2[4]);
    tab2[4] = 6;
    printint(tab2[4]);
    printint(tab[4]);
}

int *unsurdeux(int *tab, int n)
{
    int newLength = n % 2 == 0 ? n / 2 : n / 2 + 1;
    int *res = malloc(sizeof(int) * newLength);

    for (int i = 0; i < newLength; i++)
    {
        res[i] = tab[2 * i];
    }

    return res;
}

void question8()
{
    int tab[5] = {1, 2, 3, 4, 5};
    int *newtab = unsurdeux(tab, 5);
    printinttab(newtab, 3);
}

struct Matrice
{
    int nb_lignes;
    int nb_colonnes;
    int **valeurs;
};

void affiche(struct Matrice une_matrice)
{
    printf("{\n");
    for (int i = 0; i < une_matrice.nb_lignes; i++)
    {
        for (int j = 0; j < une_matrice.nb_colonnes - 1; j++)
            printf("%3i, ", une_matrice.valeurs[i][j]);
        printf("%3i\n", une_matrice.valeurs[i][une_matrice.nb_colonnes - 1]);
    }
    printf("}");
}

struct Matrice matrice(int nbl, int nbc, int *valeurs)
{
    int **val = malloc(nbl * sizeof(int*));
    for (int i = 0; i < nbl; i++)
    {
        val[i] = (int *) malloc(nbc * sizeof(int));
        for (int j = 0; j < nbc; j++)
        {
            val[i][j] = valeurs[i * nbc + j];
        }
    }
    struct Matrice res =
        {
            .nb_colonnes = nbc,
            .nb_lignes = nbl,
            .valeurs = val,
        };
    return res;
}

void question11()
{
    int v1[12] = {
        1,
        2,
        3,
        4,
        2,
        4,
        6,
        8,
        3,
        6,
        9,
        12,
    };
    struct Matrice m1 = matrice(3, 4, v1);
    affiche(m1);
}

void efface(struct Matrice uneMatrice)
{
    for (int i = 0; i < uneMatrice.nb_lignes; i++)
        free(uneMatrice.valeurs[i]);
    free(uneMatrice.valeurs);
}

struct Matrice multiplie(struct Matrice m1, struct Matrice m2)
{
    int *valeurs = malloc(sizeof(int) * m1.nb_lignes * m2.nb_colonnes);
    struct Matrice result = matrice(m1.nb_lignes, m2.nb_colonnes, valeurs);
    for (int i = 0; i < result.nb_lignes; i++)
        for (int j = 0; j < result.nb_colonnes; j++)
        {
            result.valeurs[i][j] = 0;
            for (int k = 0; k < result.nb_colonnes; k++)
                result.valeurs[i][j] += m1.valeurs[i][k] * m2.valeurs[k][j];
        }
    return result;
}

void question12()
{
    int val1[4] = {1, 2, 3, 4};
    int val2[4] = {2, 2, 2, 2};
    struct Matrice m1 = matrice(2, 2, val1);
    struct Matrice m2 = matrice(2, 2, val2);
    struct Matrice result = multiplie(m1, m2);
    affiche(result);
}

int main()
{
    int val1[4] = {1, 2, 3, 4};
    struct Matrice m1 = matrice(2, 2, val1);
    affiche(m1);
    efface(m1);
    affiche(m1);
}